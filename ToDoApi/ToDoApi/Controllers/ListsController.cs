﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using ToDoApi.DB;
using ToDoApi.Models;

namespace ToDoApi.Controllers
{
    public class ListsController : ApiController
    {
        private readonly ITodoListDbContext _db = new TodoListDbContext();

        public ListsController() { }

        public ListsController(ITodoListDbContext context)
        {
            _db = context;
        }

        // GET: api/lists
        public HttpResponseMessage GetTodoLists(string searchString = null, int? skip = null, int? limit = null)
        {
            if (skip.HasValue && skip.Value < 0 || limit.HasValue && limit.Value < 0)
            {
                return BadRequest("bad input parameter");
            }

            IQueryable<DbTodoList> dbLists = _db.TodoLists.Include("Tasks").OrderBy(l => l.Id);
            

            if (searchString != null)
            {
                dbLists = dbLists.Where(l => l.Name.Contains(searchString));
            }

            if (skip.HasValue && skip.Value > 0)
            {
                dbLists = dbLists.Skip(skip.Value);
            }

            if (limit.HasValue && limit.Value > -1)
            {
                dbLists = dbLists.Take(limit.Value);
            }

            var lists = new List<TodoList>();
            foreach (var dbList in dbLists)
            {
                lists.Add(dbList.ToTodoList());
            }

            return Ok("search results matching criteria", lists);
        }

        // GET: api/list/{listId}
        [Route("api/list/{sId}")]
        public HttpResponseMessage GetTodoList(string sId)
        {
            if (!Guid.TryParse(sId, out var id))
            {
                return BadRequest("Invalid id supplied");
            }

            var dbTodoList = _db.TodoLists.Include("Tasks").FirstOrDefault(tl => tl.Id == id);

            if (dbTodoList == null)
            {
                return NotFound();
            }

            return Ok("successful operation", dbTodoList.ToTodoList());
        }

        // POST: api/lists
        public HttpResponseMessage PostTodoList(TodoList todoList)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (TodoListExists(todoList.id))
            {
                return Conflict();
            }

            var dbTodoList = new DbTodoList(todoList);
            _db.TodoLists.Add(dbTodoList);

            foreach (var dbTask in dbTodoList.Tasks)
            {
                _db.Tasks.Add(dbTask);
            }

            _db.SaveChanges();

            return Created();
        }


        //POST: list/{listId}/tasks
        [Route("api/list/{listId}/tasks")]
        public HttpResponseMessage PostNewTaskToList(string listId, Task task)
        {
            if (!ModelState.IsValid || !TodoListExists(listId))
            {
                return BadRequest();
            }

            if (TaskExists(task.id))
            {
                return Conflict();
            }

            var dbTask = new DbTask(task, Guid.Parse(listId));

            _db.Tasks.Add(dbTask);
            _db.SaveChanges();

            return Created();
        }

        //POST: list/{listId}/task/{taskId}/complete
        [Route("api/list/{listId}/task/{taskId}/complete")]
        public HttpResponseMessage PostCompletedStateOfTask(string listId, string taskId, CompletedTask completedTask)
        {
            if (!ModelState.IsValid || !TodoListExists(listId) || !TaskExists(taskId))
            {
                return BadRequest();
            }

            var dbTaskId = Guid.Parse(taskId);

            var dbTask = _db.Tasks.First(t => t.Id == dbTaskId);
            dbTask.Completed = completedTask.completed;

            _db.MarkAsModified(dbTask);

            _db.SaveChanges();
            
            return Created("item updated");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TodoListExists(string sId)
        {
            return Guid.TryParse(sId, out var id) && _db.TodoLists.Any(e => e.Id == id);
        }

        private bool TaskExists(string sId)
        {
            return Guid.TryParse(sId, out var id) && _db.Tasks.Any(e => e.Id == id);
        }

        #region Responses

        private new HttpResponseMessage BadRequest(string reasonPhrase = "invalid input, object invalid")
        {
            return new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.BadRequest,
                ReasonPhrase = reasonPhrase,
                RequestMessage = Request
            };
        }

        private HttpResponseMessage Created(string reasonPhrase = "item created")
        {
            return new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.Created,
                ReasonPhrase = reasonPhrase,
                RequestMessage = Request
            };
        }

        private HttpResponseMessage NotFound(string reasonPhrase = "List not found")
        {
            return new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.NotFound,
                ReasonPhrase = reasonPhrase,
                RequestMessage = Request
            };
        }

        private HttpResponseMessage Conflict(string reasonPhrase = "an existing item already exists")
        {
            return new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.Conflict,
                ReasonPhrase = reasonPhrase,
                RequestMessage = Request
            };
        }

        private HttpResponseMessage Ok<T>(string reasonPhrase, T obj)
        {
            return new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                ReasonPhrase = reasonPhrase,
                Content = new ObjectContent<T>(obj, new JsonMediaTypeFormatter()),
                RequestMessage = Request
            };
        }

        #endregion
    }
}