﻿using System.Data.Entity;

namespace ToDoApi.DB
{
    public class TodoListDbInitializer: DropCreateDatabaseAlways<TodoListDbContext>
    {
        protected override void Seed(TodoListDbContext context)
        {
            base.Seed(context);
        }
    }
}