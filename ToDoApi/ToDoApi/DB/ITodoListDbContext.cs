﻿using System;
using System.Data.Entity;

namespace ToDoApi.DB
{
    public interface ITodoListDbContext : IDisposable
    {
        DbSet<DbTodoList> TodoLists { get; }
        DbSet<DbTask> Tasks { get; }

        int SaveChanges();

        void MarkAsModified(DbTodoList item);
        void MarkAsModified(DbTask item);


    }
}