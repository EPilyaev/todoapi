﻿using System;
using System.ComponentModel.DataAnnotations;
using ToDoApi.Models;

namespace ToDoApi.DB
{
    public class DbTask
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public bool Completed { get; set; }

        
        public Guid TodoListId { get; set; }
        public DbTodoList TodoList { get; set; }

        public DbTask() { }

        public DbTask(Task task, Guid todoListId)
        {
            Id = Guid.Parse(task.id);
            Name = task.name;
            Completed = task.completed;
            TodoListId = todoListId;
        }

        public Task ToTask()
        {
            return new Task(this);
        }
    }
}