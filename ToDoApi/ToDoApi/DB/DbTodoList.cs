﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ToDoApi.Models;

namespace ToDoApi.DB
{
    public class DbTodoList
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public List<DbTask> Tasks { get; set; } 

        public DbTodoList() { }

        public DbTodoList(TodoList todoList)
        {
            Id = Guid.Parse(todoList.id);
            Name = todoList.name;
            Description = todoList.description;
            Tasks = todoList.tasks.Select(t=>new DbTask(t, Id)).ToList();
        }

        public TodoList ToTodoList()
        {
            return new TodoList(this);
        }
    }
}