﻿using System.Data.Entity;

namespace ToDoApi.DB
{
    public class TodoListDbContext : DbContext, ITodoListDbContext
    {
        public DbSet<DbTodoList> TodoLists { get; set; }
        public DbSet<DbTask> Tasks { get; set; }

        public void MarkAsModified(DbTodoList item)
        {
            Entry(item).State = EntityState.Modified;
        }

        public void MarkAsModified(DbTask item)
        {
            Entry(item).State = EntityState.Modified;
        }
    }
}