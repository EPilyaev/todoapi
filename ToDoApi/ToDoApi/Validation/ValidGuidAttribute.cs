﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace ToDoApi.Validation
{
    public class ValidGuidAttribute : ValidationAttribute
    {
        private const string DefaultErrorMessage = "'{0}' does not contain a valid guid";

        public ValidGuidAttribute() : base(DefaultErrorMessage)
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var input = Convert.ToString(value, CultureInfo.CurrentCulture);
            
            if (string.IsNullOrWhiteSpace(input))
            {
                return null;
            }

            return Guid.TryParse(input, out _) ?
                null :
                new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }
    }
}