﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ToDoApi.DB;
using ToDoApi.Validation;

namespace ToDoApi.Models
{
    public class TodoList
    {
        [Required]
        [ValidGuid]
        public string id { get; set; }
        [Required]
        public string name { get; set; }
        public string description { get; set; }
        public List<Task> tasks { get; set; } = new List<Task>();

        public TodoList() { }

        public TodoList(DbTodoList dbTodoList)
        {
            id = dbTodoList.Id.ToString();
            name = dbTodoList.Name;
            description = dbTodoList.Description;
            tasks = dbTodoList.Tasks.Select(t => new Task(t)).ToList();
        }

        public override bool Equals(object o)
        {
            if (!(o is TodoList toDoList)) return false;

            var result = id.Equals(toDoList.id);
            result &= name.Equals(toDoList.name);
            result &= description.Equals(toDoList.description);
            return tasks.Aggregate(result, (current, task) => current & toDoList.tasks.Any(t => t.Equals(task)));
        }

        public override int GetHashCode()
        {
            var hashCode = 269223108;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(id);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(description);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Task>>.Default.GetHashCode(tasks);
            return hashCode;
        }
    }
}