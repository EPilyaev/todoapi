﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ToDoApi.DB;
using ToDoApi.Validation;

namespace ToDoApi.Models
{
    public class Task
    {
        [Required]
        [ValidGuid]
        public string id { get; set; }
        [Required]
        public string name { get; set; }
        public bool completed { get; set; }

        public Task() { }

        public Task(DbTask dbTask)
        {
            id = dbTask.Id.ToString();
            name = dbTask.Name;
            completed = dbTask.Completed;
        }

        public override bool Equals(object obj)
        {
            return obj is Task task &&
                   id == task.id &&
                   name == task.name &&
                   completed == task.completed;
        }

        public override int GetHashCode()
        {
            var hashCode = 1410060744;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(id);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(name);
            hashCode = hashCode * -1521134295 + completed.GetHashCode();
            return hashCode;
        }
    }
}