﻿using System.ComponentModel.DataAnnotations;

namespace ToDoApi.Models
{
    public class CompletedTask
    {
        [Required]
        public bool completed { get; set; }
    }
}