﻿using System.Data.Entity;
using System.Web.Http;
using ToDoApi.DB;

namespace ToDoApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(new TodoListDbInitializer());
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
