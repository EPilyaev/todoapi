﻿using System.Data.Entity;
using ToDoApi.DB;

namespace ToDoApi.Tests
{
    public class TestTodoListDbContext : ITodoListDbContext
    {
        public TestTodoListDbContext()
        {
            TodoLists = new TestTodoListDbSet();
            Tasks = new TestTaskDbSet();
        }

        public DbSet<DbTodoList> TodoLists { get; set; }
        public DbSet<DbTask> Tasks { get; set; }

        public int SaveChanges()
        {
            return 0;
        }

        public void MarkAsModified(DbTodoList item) { }
        public void MarkAsModified(DbTask item) { }

        public void Dispose() { }
    }
}
