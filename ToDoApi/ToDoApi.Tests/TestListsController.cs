﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using ToDoApi.Controllers;
using ToDoApi.Models;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace ToDoApi.Tests
{
    [TestFixture]
    public class TestListsController
    {
        private readonly Random _random = new Random();

        [Test]
        public void PostList_ShouldReturnCreated()
        {
            var controller = new ListsController(new TestTodoListDbContext());

            var list = GetRandomDemoList();

            var result = controller.PostTodoList(list);
            
            AssertCreated(result);
        }

        [Test]
        public void PostSameListTwice_ShouldReturnConflict()
        {
            var controller = new ListsController(new TestTodoListDbContext());

            var list = GetRandomDemoList();

            var result = controller.PostTodoList(list);

            AssertCreated(result);

            var newResult = controller.PostTodoList(list);
           
            AssertConflict(newResult);
        }

        [Test]
        public void PostIncorrectListState_ShouldReturnError()
        {
            var controller = new ListsController(new TestTodoListDbContext());
            controller.ModelState.AddModelError("","Error");

            var list = new TodoList() { id = Guid.NewGuid().ToString(), name = null };

            var result = controller.PostTodoList(list);

            AssertBadRequest(result);
        }

        [Test]
        public void PostListGetListById_ShouldReturnSameList()
        {
            var controller = new ListsController(new TestTodoListDbContext());

            var list = GetRandomDemoList();

            var result = controller.PostTodoList(list);

            AssertCreated(result);

            var getResult = controller.GetTodoList(list.id);

            AssertOk(getResult, "successful operation");

            var gotList = (TodoList)((ObjectContent) getResult.Content).Value;
            Assert.AreEqual(list, gotList);
        }

        [Test]
        public void GetListByIncorrectListState_ShouldReturnError()
        {
            var controller = new ListsController(new TestTodoListDbContext());
            controller.ModelState.AddModelError("", "Error");

            var getResult = controller.GetTodoList(Guid.NewGuid().ToString());

            Assert.IsNotNull(getResult);
            Assert.AreEqual(getResult.StatusCode, HttpStatusCode.NotFound);
            Assert.AreEqual(getResult.ReasonPhrase, "List not found");
        }

        [Test]
        public void PostDemoListsGetListsFilterLists_ShouldReturnCorrectData()
        {
            var controller = new ListsController(new TestTodoListDbContext());
            var lists = GetRandomDemoLists(_random.Next(10, 20));
            HttpResponseMessage result;

            foreach (var todoList in lists)
            {
                result = controller.PostTodoList(todoList);
                AssertCreated(result);
            }

            result = controller.GetTodoLists();
            AssertOk(result, "search results matching criteria");

            var gotLists = (List<TodoList>)((ObjectContent)result.Content).Value;

            foreach (var todoList in lists)
            {
                Assert.IsTrue(gotLists.Contains(todoList));
            }
            
            result = controller.GetTodoLists("1", 1, 2);
            AssertOk(result, "search results matching criteria");

            var filtered = lists.OrderBy(l => l.id).Where(l => l.name.Contains("1")).Skip(1).Take(2).ToList();
            var gotFiltered = ((List<TodoList>)((ObjectContent)result.Content).Value).ToList();
            Assert.IsTrue(filtered.All(gotFiltered.Contains) && filtered.Count == gotFiltered.Count);
        }

        [Test]
        public void GetListsBadParameters_ShouldReturnError()
        {
            var controller = new ListsController(new TestTodoListDbContext());

            var result =  controller.GetTodoLists("1", -1, 2);
            AssertBadRequest(result, "bad input parameter");

            result = controller.GetTodoLists("1", 1, -2);
            AssertBadRequest(result, "bad input parameter");

            result = controller.GetTodoLists("1", -1, -2);
            AssertBadRequest(result, "bad input parameter");
        }

        [Test]
        public void PostTask_ShouldReturnCreated()
        {
            var controller = new ListsController(new TestTodoListDbContext());

            var list = GetRandomDemoList();

            var result = controller.PostTodoList(list);

            AssertCreated(result);

            var task = GetRandomDemoTask();

            var taskResult = controller.PostNewTaskToList(list.id, task);

            AssertCreated(taskResult);
        }

        [Test]
        public void PostIncorrectTaskState_ShouldReturnError()
        {
            var controller = new ListsController(new TestTodoListDbContext());

            var list = GetRandomDemoList();

            var result = controller.PostTodoList(list);

            AssertCreated(result);

            var task = GetRandomDemoTask();
            task.name = "";
            controller.ModelState.AddModelError("","Error");

            var taskResult = controller.PostNewTaskToList(list.id, task);

            AssertBadRequest(taskResult);
        }

        [Test]
        public void PostTaskToRandomListId_ShouldReturnError()
        {
            var controller = new ListsController(new TestTodoListDbContext());

            var task = GetRandomDemoTask();
            var randomListId = Guid.NewGuid().ToString();

            var taskResult = controller.PostNewTaskToList(randomListId, task);

            AssertBadRequest(taskResult);
        }

        [Test]
        public void PostSameTaskTwice_ShouldReturnConflict()
        {
            var controller = new ListsController(new TestTodoListDbContext());

            var list = GetRandomDemoList();

            var result = controller.PostTodoList(list);

            AssertCreated(result);

            var task = GetRandomDemoTask();

            var taskResult = controller.PostNewTaskToList(list.id, task);

            AssertCreated(taskResult);

            taskResult = controller.PostNewTaskToList(list.id, task);

            AssertConflict(taskResult);
        }

        [Test]
        public void ChangeTaskStatus_ShouldReturnCreated()
        {
            var controller = new ListsController(new TestTodoListDbContext());

            var list = GetRandomDemoList();

            var result = controller.PostTodoList(list);

            AssertCreated(result);

            var task = GetRandomDemoTask();

            var taskResult = controller.PostNewTaskToList(list.id, task);

            AssertCreated(taskResult);

            var completedTask = new CompletedTask() { completed = _random.Next(2) == 1 };
            var taskStatusResult =
                controller.PostCompletedStateOfTask(list.id, task.id,
                        completedTask);

            AssertCreated(taskStatusResult, "item updated");
        }

        [Test]
        public void ChangeTaskStatusIncorrectList_ShouldReturnError()
        {
            var controller = new ListsController(new TestTodoListDbContext());
            

            var completedTask = new CompletedTask() { completed = _random.Next(2) == 1 };
            var taskStatusResult =
                controller.PostCompletedStateOfTask(Guid.NewGuid().ToString(), Guid.NewGuid().ToString(),
                    completedTask);

            AssertBadRequest(taskStatusResult);
        }

        private TodoList GetRandomDemoList()
        {
            return new TodoList()
            {
                id = Guid.NewGuid().ToString(),
                description = $"Demo description {_random.Next(100)}",
                name = $"Some name {_random.Next(100)}",
                tasks = new List<Task>() { new Task() { id = Guid.NewGuid().ToString(), name = $"some task name {_random.Next(100)}", completed = _random.Next(2) == 1 } }
            };
        }

        private List<TodoList> GetRandomDemoLists(int count)
        {
            var ret = new List<TodoList>();
            for (int i = 0; i < count; i++)
            {
                ret.Add(GetRandomDemoList());
            }
            return ret;
        }

        private Task GetRandomDemoTask()
        {
            return new Task()
            {
                id = Guid.NewGuid().ToString(),
                name = $"some task name {_random.Next(100)}",
                completed = _random.Next(2) == 1
            };
        }

        private void AssertCreated(HttpResponseMessage message, string reasonPhrase = "item created")
        {
            Assert.IsNotNull(message);
            Assert.IsTrue(message.StatusCode == HttpStatusCode.Created);
            Assert.IsTrue(message.ReasonPhrase == reasonPhrase);
        }

        private void AssertOk(HttpResponseMessage message, string reasonPhrase)
        {
            Assert.IsNotNull(message);
            Assert.IsTrue(message.StatusCode == HttpStatusCode.OK);
            Assert.IsTrue(message.ReasonPhrase == reasonPhrase);
        }

        private void AssertConflict(HttpResponseMessage message, string reasonPhrase = "an existing item already exists")
        {
            Assert.IsNotNull(message);
            Assert.IsTrue(message.StatusCode == HttpStatusCode.Conflict);
            Assert.IsTrue(message.ReasonPhrase == reasonPhrase);
        }

        private void AssertBadRequest(HttpResponseMessage message, string reasonPhrase = "invalid input, object invalid")
        {
            Assert.IsNotNull(message);
            Assert.IsTrue(message.StatusCode == HttpStatusCode.BadRequest);
            Assert.IsTrue(message.ReasonPhrase == reasonPhrase);
        }
    }
}
