﻿using System;
using System.Linq;
using ToDoApi.DB;

namespace ToDoApi.Tests
{
    class TestTodoListDbSet : TestDbSet<DbTodoList>
    {
        public override DbTodoList Find(params object[] keyValues)
        {
            return this.SingleOrDefault(todoList => todoList.Id == (Guid)keyValues.Single());
        }
    }
}
