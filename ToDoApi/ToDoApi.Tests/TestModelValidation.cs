﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ToDoApi.Models;
using Task = ToDoApi.Models.Task;

namespace ToDoApi.Tests
{
    [TestClass]
    public class TestModelValidation
    {
        [TestMethod]
        public void TodoList_Valid()
        {
            var model = new TodoList
            {
                id = Guid.NewGuid().ToString(),
                name = "some name"
            };

            var context = new ValidationContext(model);
            var result = new List<ValidationResult>();

            var valid = Validator.TryValidateObject(model, context, result, true);

            Assert.IsTrue(valid);
        }

        [TestMethod]
        public void TodoList_NameRequired()
        {
            void CheckName(TodoList model)
            {
                var context = new ValidationContext(model);
                var result = new List<ValidationResult>();

                var valid = Validator.TryValidateObject(model, context, result, true);

                Assert.IsFalse(valid);
                var errorMes = result.FirstOrDefault(x => x.ErrorMessage == "The name field is required.");
                Assert.IsNotNull(errorMes);
                var member = errorMes.MemberNames.FirstOrDefault(x => x == "name");
                Assert.IsNotNull(member);
            }

            var list = new TodoList
            {
                id = Guid.NewGuid().ToString(),
                name = ""
            };

            CheckName(list);

            list = new TodoList
            {
                id = Guid.NewGuid().ToString(),
                name = null
            };

            CheckName(list);
        }

        [TestMethod]
        public void TodoList_IdRequired()
        {
            void CheckId(TodoList model)
            {
                var context = new ValidationContext(model);
                var result = new List<ValidationResult>();

                var valid = Validator.TryValidateObject(model, context, result, true);

                Assert.IsFalse(valid);
                var errorMes = result.FirstOrDefault(x => x.ErrorMessage == "The id field is required.");
                Assert.IsNotNull(errorMes);
                var member = errorMes.MemberNames.FirstOrDefault(x => x == "id");
                Assert.IsNotNull(member);
            }

            var list = new TodoList
            {
                name = "someName",
                id = ""
            };

            CheckId(list);

            list = new TodoList
            {
                name = "someName",
                id = null
            };

            CheckId(list);
        }

        [TestMethod]
        public void TodoList_IdInvalid()
        {
            var model = new TodoList
            {
                name = "someName",
                id = "a123ds"
            };

            var context = new ValidationContext(model);
            var result = new List<ValidationResult>();

            var valid = Validator.TryValidateObject(model, context, result, true);

            Assert.IsFalse(valid);
            var errorMes = result.FirstOrDefault(x => x.ErrorMessage == "'id' does not contain a valid guid");
            Assert.IsNotNull(errorMes);
        }

        [TestMethod]
        public void Task_Valid()
        {
            var model = new Task
            {
                id = Guid.NewGuid().ToString(),
                name = "some name"
            };

            var context = new ValidationContext(model);
            var result = new List<ValidationResult>();

            var valid = Validator.TryValidateObject(model, context, result, true);

            Assert.IsTrue(valid);
        }

        [TestMethod]
        public void Task_NameRequired()
        {
            void CheckName(Task model)
            {
                var context = new ValidationContext(model);
                var result = new List<ValidationResult>();

                var valid = Validator.TryValidateObject(model, context, result, true);

                Assert.IsFalse(valid);
                var errorMes = result.FirstOrDefault(x => x.ErrorMessage == "The name field is required.");
                Assert.IsNotNull(errorMes);
                var member = errorMes.MemberNames.FirstOrDefault(x => x == "name");
                Assert.IsNotNull(member);
            }

            var task = new Task
            {
                id = Guid.NewGuid().ToString(),
                name = ""
            };

            CheckName(task);

            task = new Task
            {
                id = Guid.NewGuid().ToString(),
                name = null
            };

            CheckName(task);
        }

        [TestMethod]
        public void Task_IdRequired()
        {
            void CheckId(Task model)
            {
                var context = new ValidationContext(model);
                var result = new List<ValidationResult>();

                var valid = Validator.TryValidateObject(model, context, result, true);

                Assert.IsFalse(valid);
                var errorMes = result.FirstOrDefault(x => x.ErrorMessage == "The id field is required.");
                Assert.IsNotNull(errorMes);
                var member = errorMes.MemberNames.FirstOrDefault(x => x == "id");
                Assert.IsNotNull(member);
            }

            var task = new Task
            {
                name = "someName",
                id = ""
            };

            CheckId(task);

            task = new Task
            {
                name = "someName",
                id = null
            };

            CheckId(task);
        }

        [TestMethod]
        public void Task_IdInvalid()
        {
            var model = new Task
            {
                name = "someName",
                id = "a123ds"
            };

            var context = new ValidationContext(model);
            var result = new List<ValidationResult>();

            var valid = Validator.TryValidateObject(model, context, result, true);

            Assert.IsFalse(valid);
            var errorMes = result.FirstOrDefault(x => x.ErrorMessage == "'id' does not contain a valid guid");
            Assert.IsNotNull(errorMes);
        }
    }
}
