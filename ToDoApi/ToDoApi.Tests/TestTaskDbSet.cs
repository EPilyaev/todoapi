﻿using System;
using System.Linq;
using ToDoApi.DB;

namespace ToDoApi.Tests
{
    class TestTaskDbSet : TestDbSet<DbTask>
    {
        public override DbTask Find(params object[] keyValues)
        {
            return this.SingleOrDefault(task => task.Id == (Guid)keyValues.Single());
        }
    }
}
