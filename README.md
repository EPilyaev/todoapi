When cloned, all references will be added during the build.

Simply start the solution and access API. 

You can choose to select "Don't open a page. Wait a request from an external application." at Project Settings/Web to not to get an error page while starting with browser. 